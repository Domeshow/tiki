<?php

// (c) Copyright by authors of the Tiki Wiki CMS Groupware Project
//
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.
class Search_Formatter_Plugin_SmartyTemplate implements Search_Formatter_Plugin_Interface
{
    private $templateFile;
    private $changeDelimiters;
    private $data = [];
    private $fields = [];
    private string $editable;
    private string $editableId;
    private ?string $context;

    public function __construct($templateFile, $changeDelimiters = false)
    {
        $this->templateFile = $templateFile;
        $this->changeDelimiters = (bool) $changeDelimiters;
        $this->editable = '';
    }

    public function setData(array $data)
    {
        $this->data = $data;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function setFields(array $fields)
    {
        $this->fields = $fields;
    }

    public function setEditable(string $editable, string $id)
    {
        $this->editable = $editable;
        $this->editableId = $id;
    }

    public function setContext(?string $context)
    {
        $this->context = $context;
    }

    public function getFormat()
    {
        return self::FORMAT_HTML;
    }

    public function prepareEntry($entry)
    {
        return $entry->getPlainValues();
    }

    public function renderEntries(Search_ResultSet $entries)
    {
        if ($this->editable && isset($_REQUEST[$this->editableId])) {
            return $this->wrapEditableByContext($_REQUEST[$this->editableId]);
        }

        $smarty = new Smarty_Tiki();

        if ($this->changeDelimiters) {
            $smarty->setLeftDelimiter('{{');
            $smarty->setRightDelimiter('}}');
        }

        foreach ($this->data as $key => $value) {
            $smarty->assign($key, $value);
        }

        $smarty->assign('base_url', $GLOBALS['base_url']);
        $smarty->assign('prefs', $GLOBALS['prefs']);
        $smarty->assign('user', $GLOBALS['user']);
        $smarty->assign('results', $entries);
        $smarty->assign(
            'facets',
            array_map(
                function ($facet) {
                    return array_filter([
                        'name' => $facet->getName(),
                        'label' => $facet->getLabel(),
                        'options' => $facet->getOptions(),
                        'operator' => $facet->getOperator(),
                    ]);
                },
                $entries->getFacets()
            )
        );
        $smarty->assign('count', count($entries));
        $smarty->assign('offset', $entries->getOffset());
        $smarty->assign('offsetplusone', $entries->getOffset() + 1);
        $smarty->assign('offsetplusmaxRecords', $entries->getOffset() + $entries->getMaxRecords());
        $smarty->assign('maxRecords', $entries->getMaxRecords());
        $smarty->assign('id', $entries->getId());
        $smarty->assign('tsOn', $entries->getTsOn());
        $tsettings = $entries->getTsSettings();
        if (is_array($tsettings) && isset($tsettings['math'])) {
            $smarty->assign('tstotals', $tsettings['math']['totals']);
            $smarty->assign('tscols', $tsettings['columns']);
        }
        global $jitRequest;
        if (! empty($jitRequest)) {
            $adddata = $jitRequest->adddata->text();
            if (! is_null($adddata)) {
                $smarty->assign('adddata', json_decode($adddata, true));
            }
        }

        $r = $smarty->fetch($this->templateFile);

        if ($this->editable) {
            $r = $this->wrapEditableByContext($r);
        }

        return $r;
    }

    private function wrapEditableByContext($content)
    {
        if ($this->context !== 'actions') {
            return new Tiki_Render_Editable(
                $content,
                [
                    'layout' => $this->editable,
                    'field' => [
                        'id' => $this->editableId,
                        'type' => 'form',
                        'wysiwyg' => true,
                    ],
                ],
            );
        } else {
            return $content;
        }
    }
}
